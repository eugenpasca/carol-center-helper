<?php
/**
 * Slide Shortcode Template
 *
 * PHP version 7
 *
 * @category  PHP
 * @package   CarolCenterHelper
 * @author    Eugen Pasca <eugen@eugenpasca.rocks>
 * @copyright 2018 inSegment
 *
 * @var string $subtitle_above The subtitle displayed above the main title
 * @var string $title          The title of the slide
 * @var string $image          The ID of the background image for the slide
 * @var string $imageUrl       The Image URL for the background of the slide
 * @var array  $cta_link       The array having the infor for the Call to Action. ['url', 'title', 'target', 'rel' ]
 * @var string $el_class       The custom class set up for the current slider
 *
 */
?>

<div class="slide <?php echo esc_attr($el_class);?>">

</div>
