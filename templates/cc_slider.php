<?php
/**
 * Slider Shortcode Template
 *
 * PHP version 7
 *
 * @category  PHP
 * @package   CarolCenterHelper
 * @author    Eugen Pasca <eugen@eugenpasca.rocks>
 * @copyright 2018 inSegment
 *
 * @var string $el_class  class of the slider
 * @var string $sliderId  unique id for the slider. This is generated every load
 * @var string $css_class css class created by the desing options
 *
 */
?>

<div id="<?php echo $sliderId;?>" class="slider <?php echo esc_attr($css_class);?> <?php echo esc_attr($el_class);?>">
    <?php echo do_shortcode($content); ?>
</div>