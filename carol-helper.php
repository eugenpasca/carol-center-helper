<?php
/**
 * Plugin Name: Carol Center Helper
 * Plugin URI: http://insegment.com/
 * Description: Add custom functionality to the website
 * Version: 1.0.0
 * Author: Eugen Pasca
 * Author URI: https://eugenpasca.rocks/
 *
 */

if (!defined('CC_VER')) {
    define('CC_VER', '1.0.0');
}

define('CD_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('CD_PLUGIN_URL', plugin_dir_url(__FILE__));

add_action('vc_before_init', 'sliderShortcodeExtender', 10);
function sliderShortcodeExtender()
{
    //Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
    if (class_exists('WPBakeryShortCodesContainer')) {
        class WPBakeryShortCode_Cc_Slider extends WPBakeryShortCodesContainer
        {
        }
    }
    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Cc_Slide extends WPBakeryShortCode
        {
        }
    }
}

// Start up the engine
class CcHelper
{

    /**
     * Static property to hold our singleton instance
     *
     */
    public static $instance = false;

    /**
     * This is our constructor
     *
     * @return void
     */
    private function __construct()
    {
        // back end
        add_action('plugins_loaded', [ $this, 'textdomain' ]);
        add_action('vc_before_init', [ $this, 'sliderMapper' ], 15);

        // Shortcodes
        // Function to add resources shortcode

        add_shortcode('cc_slider', [ $this, 'sliderShortcode' ]);
        add_shortcode('cc_slide', [ $this, 'slideShortcode' ]);
    }

    /**
     * If an instance exists, this returns it.  If not, it creates one and
     * retuns it.
     *
     * @return CcHelper
     */

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * load textdomain
     *
     * @return void
     */

    public function textdomain()
    {
        load_plugin_textdomain('CC', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    }

    /**
     * Map shortcodes for the slider
     *
     * @return void
     */
    public function sliderMapper()
    {

        //Register "container" content element. It will hold all your inner (child) content elements
        vc_map([
            "name" => __("Carol Center Slider", "CC"),
            "base" => "cc_slider",
            "as_parent" => ['only' => 'cc_slide'], // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
            "content_element" => true,
            "show_settings_on_create" => false,
            "is_container" => true,
            "params" => [
                // add params same as with any other content element
                [
                    "type" => "textfield",
                    "heading" => __("Extra class name", "CC"),
                    "param_name" => "el_class",
                    "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "CC")
                ],
                [
                    'type' => 'css_editor',
                    'heading' => __('Css', "CC"),
                    'param_name' => 'css',
                    'group' => __('Design options', "CC"),
                ]
            ],
            "js_view" => 'VcColumnView'
        ]);
        vc_map([
            "name" => __("Slide", "CC"),
            "base" => "cc_slide",
            "content_element" => true,
            "as_child" => ['only' => 'cc_slider'], // Use only|except attributes to limit parent (separate multiple values with comma)
            "params" => [
                // add params same as with any other content element
                [
                    "type" => "textfield",
                    "heading" => __("Subtitle", "CC"),
                    "param_name" => "subtitle_above",
                    "description" => __("Add the subtitle placed above the slider heading .", "CC")
                ],
                [
                    "type" => "textfield",
                    "heading" => __("Title", "CC"),
                    "param_name" => "title",
                    "description" => __("Add the slide title.", "CC")
                ],
                [
                    "type" => "attach_image",
                    "heading" => __("Slide image", "CC"),
                    "param_name" => "image",
                    "description" => __("Add the slide background.", "CC")
                ],
                [
                    "type" => "vc_link",
                    "heading" => __("CTA button", "CC"),
                    "param_name" => "cta_link",
                    "description" => __("Set the CTA Button. Leave empty to hide it", "CC")
                ],
                [
                    "type" => "textfield",
                    "heading" => __("Extra class name", "CC"),
                    "param_name" => "el_class",
                    "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "CC")
                ],
            ]
        ]);
    }

    /**
     * Shortcode to display a slider
     *
     * @param array $atts
     * @param string $content
     * @return string
     */
    public function sliderShortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'el_class' => '',
            'css' => ''
        ), $atts));
        $sliderId = uniqid('cc_slider_');
        $css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '), $this->settings['base'], $atts);

        ob_start();
        require_once(CD_PLUGIN_PATH . 'templates/cc_slider.php');
        $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }


    public function slideShortcode($atts)
    {
        extract(shortcode_atts(array(
            'subtitle_above' => '',
            'title' => '',
            'image' => '',
            'cta_link' => '',
            'el_class' => '',
        ), $atts));

        $slideId = uniqid('cc_slide_');
        $imageUrl = wp_get_attachment_image_src($image, 'full', false);
        $cta_link = vc_build_link($cta_link);

        ob_start();
        require_once(CD_PLUGIN_PATH . 'templates/cc_slide.php');
        $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}


// Instantiate our class
$CcHelper = CcHelper::getInstance();
